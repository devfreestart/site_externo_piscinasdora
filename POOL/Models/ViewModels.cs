﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POOL.Models
{
    public class MessageModel
    {
        public string Name { get; set; }
        public string Radio1 { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
    }
}