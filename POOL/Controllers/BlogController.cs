﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POOL.Controllers
{
    public class BlogController : Controller
    {
        //
        // GET: /Blog/

        public ActionResult Index()
        {
            return RedirectToAction("Classic");
        }

        public ActionResult Classic()
        {
            return View();
        }

        public ActionResult Card()
        {
            return View();
        }
        public ActionResult Single()
        {
            return View();
        }
    }
}
