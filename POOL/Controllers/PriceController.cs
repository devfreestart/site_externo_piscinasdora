﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POOL.Controllers
{
    public class PriceController : Controller
    {
        //
        // GET: /Price/

        public ActionResult Index()
        {
            return RedirectToAction("Prices");
        }

        public ActionResult Prices()
        {
            return View();
        }

        public ActionResult Coupons()
        {
            return View();
        }
    }
}
