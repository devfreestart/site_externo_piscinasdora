﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using POOL.Filters;
using POOL.Models;

namespace POOL.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model , string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName , model.Password , persistCookie: model.RememberMe))
            {
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("" , "O nome de usuário ou a senha fornecidos estão incorretos.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index" , "Home");
        }


        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index" , "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess ,
            SetPasswordSuccess ,
            RemoveLoginSuccess ,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider , string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider , ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "O nome de usuário já existe. Por favor insira um nome de usuário diferente.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Um nome de usuário para esse endereço de e-mail já existe. Digite um endereço de e-mail diferente.";

                case MembershipCreateStatus.InvalidPassword:
                    return "A senha fornecida é inválida. Digite um valor de senha válido.";

                case MembershipCreateStatus.InvalidEmail:
                    return "O endereço de e-mail fornecido é inválido. Verifique o valor e tente novamente.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "A resposta de recuperação de senha fornecida não é válida. Verifique o valor e tente novamente.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "A pergunta de recuperação de senha fornecida é inválida. Verifique o valor e tente novamente.";

                case MembershipCreateStatus.InvalidUserName:
                    return "O nome de usuário fornecido é inválido. Verifique o valor e tente novamente.";

                case MembershipCreateStatus.ProviderError:
                    return "O provedor de autenticação retornou um erro. Por favor, verifique a sua entrada e tente novamente. Se o problema persistir, entre em contato com o administrador do sistema.";

                case MembershipCreateStatus.UserRejected:
                    return "O pedido de criação de usuário foi cancelado. Por favor, verifique a sua entrada e tente novamente. Se o problema persistir, entre em contato com o administrador do sistema.";

                default:
                    return "Ocorreu um erro desconhecido. Por favor, verifique a sua entrada e tente novamente. Se o problema persistir, entre em contato com o administrador do sistema.";
            }
        }
        #endregion
    }
}
