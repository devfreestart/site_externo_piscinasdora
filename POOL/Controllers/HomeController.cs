﻿using POOL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POOL.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Testimonial()
        {
            return View();
        }

        public ActionResult Gallery()
        {
            return View();
        }

        public ActionResult Tips()
        {
            return View();
        }
        public ActionResult Shop(int id = 1)
        {
            return View(id);
        }

        public ActionResult Contact()
        {
            return View();
        }

        // POST: Message
        [HttpPost]
        public ActionResult Message(MessageModel model)
        {
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.From = new System.Net.Mail.MailAddress("hotsite@piscinasdora.com", "Piscinas Dora");
                mail.To.Add("piscinasdora@gmail.com");
                mail.ReplyToList.Add(new System.Net.Mail.MailAddress(model.Email, model.Name));

                mail.Priority = System.Net.Mail.MailPriority.High; //Prioridade do email.
                mail.IsBodyHtml = true; //Ativar conteúdo html?

                string assunto = string.Empty;

                mail.Subject = "Solicitação de Contato - " + model.Name;

                if (!String.IsNullOrEmpty(model.Radio1))
                {
                    //Corpo do email.
                    switch (model.Radio1)
                    {
                        case "Piscina": assunto = "Piscinas"; break;
                        case "Materiais": assunto = "Materiais"; break;
                        case "Sauna": assunto = "Sauna"; break;
                        case "Servicos": assunto = "Serviços"; break;
                    }

                    mail.Body = "Mensagem do Hotsite <br/> Nome:  "
                        + model.Name + "<br/> Assunto : " + assunto
                        + model.Name + "<br/> Email : " + model.Email
                        + "<br/> Telefone : " + model.Phone
                        + " <br/> Mensagem : " + model.Message;

                }
                else
                {
                    //Corpo do email.
                    mail.Body = "Mensagem do Hotsite <br/> Nome:  "
                        + model.Name + "<br/> Email : " + model.Email
                        + "<br/> Telefone : " + model.Phone
                        + " <br/> Mensagem : " + model.Message;
                }

                mail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
                mail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                client.Host = "email-ssl.com.br"; //Endereço do servidor SMTP.
                client.EnableSsl = true;
                client.Port = 587;
                client.Credentials = new System.Net.NetworkCredential("hotsite@piscinasdora.com", "80pD&e#7");

                client.Send(mail);

                return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

    }
}
